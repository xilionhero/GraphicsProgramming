//***************************************************************************************
// color.fx by Frank Luna (C) 2011 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************

cbuffer cbPerObject
{
	float4x4 gWorldViewProj; 
};

struct VertexIn
{
	float3 Pos   : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    float4 Color : COLOR;
	float3 Normal : NORMAL;
};

struct GeoOut
{
	float4 PosH : SV_POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	
	// Transform to homogeneous clip space.
	vout.PosH = mul(float4(vin.Pos, 1.0f), gWorldViewProj);
	
	// Just pass vertex color into the pixel shader.
    vout.Color = vin.Color;

	// Set normal.
	vout.Normal = float3(0.0f, 1.0f, 0.0f);
    
    return vout;
}


void Subdivide(VertexOut vertexIn[3], out VertexOut vertexOut[6])
{
	VertexOut m[3];
	float4 offset = float4(1.0f, 2.0f, 0.0f, 0.0f);

	// Get midpoint of every edge.
	m[0].PosH = 0.5f*(vertexIn[0].PosH + vertexIn[1].PosH) + offset;
	m[1].PosH = 0.5f*(vertexIn[1].PosH + vertexIn[2].PosH) + offset;
	m[2].PosH = 0.5f*(vertexIn[2].PosH + vertexIn[0].PosH) + offset;
	// Set colors
	m[0].Color = vertexIn[0].Color;
	m[1].Color = vertexIn[1].Color;
	m[2].Color = vertexIn[2].Color;
	// Derive normals
	m[0].Normal = float3(0.0f, 1.0f, 0.0f);
	m[1].Normal = float3(0.0f, 1.0f, 0.0f);
	m[2].Normal = float3(0.0f, 1.0f, 0.0f);

	vertexOut[0] = vertexIn[0];
	vertexOut[1] = m[0];
	vertexOut[2] = m[2];
	vertexOut[3] = m[1];
	vertexOut[4] = vertexIn[2];
	vertexOut[5] = vertexIn[1];
}

void OutputSubdivision(VertexOut v[6], 
	inout TriangleStream<GeoOut> triStream)
{
	GeoOut gout[6];
	[unroll]
	for (int i = 0; i < 6; ++i)
	{
		gout[i].PosH = v[i].PosH;
		gout[i].Color = v[i].Color;
		gout[i].Normal = v[i].Normal;
	}

	// Create 3 triangle strips for the seperate triangles.
	triStream.Append(gout[0]);
	triStream.Append(gout[1]);
	triStream.Append(gout[2]);

	triStream.RestartStrip();

	triStream.Append(gout[1]);
	triStream.Append(gout[5]);
	triStream.Append(gout[3]);

	triStream.RestartStrip();

	triStream.Append(gout[2]);
	triStream.Append(gout[3]);
	triStream.Append(gout[4]);
}

[maxvertexcount(9)]
void GS(triangle VertexOut gin[3],
	inout TriangleStream<GeoOut> triStream)
{
	VertexOut v[6];
	Subdivide(gin, v);
	OutputSubdivision(v, triStream);
}

float4 PS(GeoOut pin) : SV_Target
{
	return pin.Color;
}

technique11 ColorTech
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( CompileShader( gs_5_0, GS() ) );
        SetPixelShader( CompileShader( ps_5_0, PS() ) );
    }
}

