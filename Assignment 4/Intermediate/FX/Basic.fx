//=============================================================================
// Basic.fx by Frank Luna (C) 2011 All Rights Reserved.
//
// Basic effect that currently supports transformations, lighting, and texturing.
//=============================================================================

#include "LightHelper.fx"
cbuffer cbPerFrame
{
	DirectionalLight gDirLights[3];
	float3 gEyePosW;

	float time;
	float  gFogStart;
	float  gFogRange;
	float4 gFogColor;
};

cbuffer cbPerObject
{
	float4x4 gWorld;
	float4x4 gWorldInvTranspose;
	float4x4 gWorldViewProj;
	float4x4 gTexTransform;
	Material gMaterial;
}; 

// Nonnumeric values cannot be added to a cbuffer.
Texture2D gDiffuseMap;

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;

	AddressU = WRAP;
	AddressV = WRAP;
};

struct VertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
};

struct VertexOut
{
	float4 PosH    : SV_POSITION;
    float3 PosW    : POSITION;
    float3 NormalW : NORMAL;
};



VertexOut VS(VertexIn vin)
{
	VertexOut vout;

	// Transform to world space space.
	vout.PosW    = mul(float4(vin.PosL, 1.0f), gWorld).xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)gWorldInvTranspose);
		
	// Transform to homogeneous clip space.
	vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);
	
	return vout;
}
 
#define R(p, a) p = p * cos(a) + float2(-p.y, p.x) * sin(a)

float Sin01(float t)
{
    return 0.5 + 0.5 * sin(6.28319 * t);
}

float SineEggCarton(float3 p)
{
    return 1.0 - abs(sin(p.x) + sin(p.y) + sin(p.z)) / 3.0;
}

float Map(float3 p, float scale)
{
    float dSphere = length(p) - 1.0;
    return max(dSphere, (0.95 - SineEggCarton(scale * p)) / scale);
}

float3 GetColor(float3 p)
{
    float amount = clamp((1.5 - length(p)) / 2.0, 0.0, 1.0);
    float3 col = 0.5 + 0.5 * cos(6.28319 * (float3(0.2, 0.0, 0.0) + amount * float3(1.0, 1.0, 0.5)));
    return col * amount;
}

float4 PS(VertexOut pin, uniform int gLightCount) : SV_Target
{
    float4 litColor;
    float2 uv = (2.0 * pin.PosH.xy - float2(800, 600)) / 800;
    float3 rd = normalize(float3(uv, -1.0));
    float3 ro = float3(0.0, 0.0, lerp(1.2, 2.0, Sin01(0.05 * time)));

    //float3 rd = -1.0 normalize(float3(2.0 * pin.PosH.xy - pin.PosH.xy, -pin.PosW.y));
    //float3 ro = float3(0.0, 0.0, lerp(1.2, 2.0, Sin01(0.05 * time)));
    R(rd.xz, 0.5 * time);
    R(ro.xz, 0.5 * time);
    R(rd.yz, 0.1 * time);
    R(ro.yz, 0.1 * time);

    float t = 0.0;
    litColor.rgb = float3(0.0, 0.0, 0.0);
    float scale = lerp(3.5, 9.0, Sin01(0.068 * time));
    for (int i = 0; i < 64; i++)
    {
        float3 p = ro + t * rd;
        float d = Map(p, scale);
        if (t > 5.0 || d < 0.001)
        {
            break;
        }
        t += 0.8 * d;
        litColor.rgb += 0.05 * GetColor(p);
    }
	// Common to take alpha from diffuse material.
	litColor.a = gMaterial.Diffuse.a;

    return litColor;
}

technique11 Light1
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(1) ) );
    }
}

technique11 Light2
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(2) ) );
    }
}

technique11 Light3
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_5_0, PS(3) ) );
    }
}
